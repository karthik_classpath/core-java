## Create a file and add some text using FileWriter
```java
public class FileHandlingDemo {
	public static void main(String[] args) throws IOException {
		FileWriter	fileWriter	=	new FileWriter("D://first.txt");
		fileWriter.write("TExt addwed");
		fileWriter.close();
	}
}
```
##  Append it
```java
fileWriter.append("Appended text");
```
##  Try BufferedWriter instead of FileWriter
```java
BufferedWriter	fileWriter	=	new	BufferedWriter(new FileWriter("D://first.txt"));
```
##  Read and print from the same file
```java
BufferedReader	reader	=	new	BufferedReader(new	FileReader("D://first.txt"));
boolean	flag	=	true;
while (flag) {
    String	lineString	=	reader.readLine();
	if(lineString != null)
		System.out.println(lineString);
	else 
		flag	=	false;
}
reader.close();
```		
##  Create a program to copy a file to "Back up"
##  write FileReader as a method and call Writer instead of printing
```java
private static	void copyFile(String path) throws IOException {
	BufferedReader	reader	=	new	BufferedReader(new	FileReader(path));
	boolean	flag	=	true;
	while (flag) {
		String	lineString	=	reader.readLine();
		if(lineString != null)
			writeToFile(lineString);
		else 
			flag	=	false;
	}
	reader.close();
}
```
##  Create writer as a method
```java
private static	void writeToFile(String	msg) throws IOException {
	BufferedWriter	fileWriter	=	new	BufferedWriter(new FileWriter("D://Backup.txt"));
	fileWriter.append(msg);
	fileWriter.close();
}
```
##  Write a main accordingly and pass the file path to be copied.
```java
Scanner	scanner	=	new	Scanner(System.in);
System.out.println("Enter the file path :");
String	pathString	=	scanner.next();
		
copyFile(pathString);
System.out.println("File copied to D://Backup.txt");
		
scanner.close();
```
------------
<br>

#   Serialization
##  Create a simple class "Student"
```java
class	Student{
	int	id;
	String	nameString;
	public Student(int	id,String	name) {
		// TODO Auto-generated constructor stub
		this.id	=	id;
		this.nameString	=	name;
	}
}
@Override
public String toString() {
	return "Student [id=" + id + ", nameString=" + nameString + "]";
}	
```
##  Write main method to record an object of this to file system.
```java
Student	student	=	new	Student(23, "Alex");
ObjectOutputStream	outputStream	=	new	ObjectOutputStream(new FileOutputStream("D://student.txt"));
outputStream.writeObject(student);
outputStream.close();
```
##  Run and verify the file
##  Get it back from file and print
```java
ObjectInputStream	inputStream	=	new	ObjectInputStream(new FileInputStream("D://student.txt"));
Student	deserializedStudent	=	(Student)	inputStream.readObject();
System.out.println(deserializedStudent);
inputStream.close();
```


