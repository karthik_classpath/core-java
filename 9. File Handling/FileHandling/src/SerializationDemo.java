import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationDemo {
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		Student	student	=	new	Student(23, "Alex");
		ObjectOutputStream	outputStream	=	new	ObjectOutputStream(new FileOutputStream("D://student.txt"));
		outputStream.writeObject(student);
		outputStream.close();
		
		ObjectInputStream	inputStream	=	new	ObjectInputStream(new FileInputStream("D://student.txt"));
		Student	deserializedStudent	=	(Student)	inputStream.readObject();
		System.out.println(deserializedStudent);
		inputStream.close();
		
	}
}
class	Student	implements	Serializable{
	int	id;
	String	nameString;
	public Student(int	id,String	name) {
		// TODO Auto-generated constructor stub
		this.id	=	id;
		this.nameString	=	name;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", nameString=" + nameString + "]";
	}
	
	
}	
