import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileHandlingDemo {
	public static void main(String[] args) throws IOException {
		Scanner	scanner	=	new	Scanner(System.in);
		System.out.println("Enter the file path :");
		String	pathString	=	scanner.next();
		
		copyFile(pathString);
		System.out.println("File copied to D://Backup.txt");
		
		scanner.close();
	}
	
	private static	void writeToFile(String	msg) throws IOException {
		BufferedWriter	fileWriter	=	new	BufferedWriter(new FileWriter("D://Backup.txt"));
		fileWriter.append(msg);
		fileWriter.close();
	}
	
	private static	void copyFile(String path) throws IOException {
		BufferedReader	reader	=	new	BufferedReader(new	FileReader(path));
		boolean	flag	=	true;
		while (flag) {
			String	lineString	=	reader.readLine();
			if(lineString != null)
				writeToFile(lineString);
			else 
				flag	=	false;
		}
		reader.close();
	}
}