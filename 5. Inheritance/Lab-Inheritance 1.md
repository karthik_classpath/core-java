class Doctor {
    public void treatPatient (){
        System.out.println("Treating patient...");
    }
}
/*
  1. Using extends keyword a class will inherit all the properties and behviour from its parent class
  2. Apart from the methods from the parent class, the child class can also have extra behaviour
  3. Super class do not know about the child class
  4. All the child class inherit from the super class
  5. Superclass and child class are relative
*/
class Dentist extends Doctor {
    public void toothImplant(){
        System.out.println("Performing tooth implant ");
    }
}
class Padietric extends Doctor {
    public void treatKids(){
        System.out.println("Treating kids ");
    }
}
class OrthoPedecian extends Doctor {
    public void conductXRay(){
        System.out.println("Conducting X-Ray ");
    }
    public void conductCTScan(){
        System.out.println("Conducting CT scan");
    }
}
class KneeSurgeon extends OrthoPedecian {
    public void conductKneeSurgery(){
        System.out.println("Conducting Knee Surgeon");
    }
}
public class DoctorClient {
    public static void main(String args[]) {

        //LHS = RHS
        // RHS should always follow IS-A relationship with LHS
        Doctor doc = new Doctor();
        OrthoPedecian ortho = new OrthoPedecian();

        //compilation error
       // OrthoPedecian ortho1 = new Dentist();
        Doctor doc2 = new OrthoPedecian();

        Doctor doc3 = new KneeSurgeon();
        OrthoPedecian doc4 = new KneeSurgeon();

        //compilation error - not a IS-A relationship
        //KneeSurgeon doc5 = new OrthoPedecian();

        //LHS is used to decided which method can be invoked
        //Only methods declared in the reference (LHS) can be invoked
        //This is during compilation
        doc.treatPatient();

        OrthoPedecian orthoPedecian = null;
        orthoPedecian.treatPatient();
        orthoPedecian.conductCTScan();

        KneeSurgeon kneeSurgeon = new KneeSurgeon();
        kneeSurgeon.conductKneeSurgery();
        kneeSurgeon.conductCTScan();
        kneeSurgeon.treatPatient();
        kneeSurgeon.conductXRay();

        //IS-A relationship
        //called upcasting - free and compiler does not crib
        Doctor doctor = new OrthoPedecian();

        //compiler will not allow becuse this is not guranteed to be true alwasy
        //OrthoPedecian orthoPedecian1 = doctor;

        //down casting - compiler will allow and can throw classcast exception during runtime
        //programmer is overriding the compilers decision to compile
        OrthoPedecian orthoPedecian1 = (OrthoPedecian) doctor;
        orthoPedecian1.conductCTScan();

        doctor = new Dentist();
        OrthoPedecian orthoPedecian2 = (OrthoPedecian) doctor;
        //throws class cast exception during runtime
        orthoPedecian2.conductCTScan();
        
        //The above can be avoid by using instanceof
        if (doctor instanceof OrthoPedecian){
            OrthoPedecian o = (OrthoPedecian) doctor;
         }
    }
}