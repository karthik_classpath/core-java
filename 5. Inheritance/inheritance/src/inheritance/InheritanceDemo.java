package inheritance;

//Subclasses and superclass related
class	Doctor{
	public void treatPatient() {
		System.out.println("Treating Patient");
	}
}

class	Dentist	extends	Doctor{
	public void toothImplant() {
		System.out.println("Implanting tooth");
	}
}

class	Pediatrician	extends	Doctor{
	public void treatKids() {
		System.out.println("Treating kidss");
	}
}

class	Ortho	extends	Doctor{
	public void conductXRay() {
		System.out.println("Conducting XRay");
	}
}

class	KneeSurgeon	extends	Ortho{
	public void kneeSurgery() {
		System.out.println("Performing knee surgery");
	}
}

class	InheritanceDemo{
	public static void main(String[] args) {
		//LHS=RHS
		//RHS should follow IS-A relationship with LHS
		Doctor	doctor=new Doctor();
		Dentist	dentist=new	Dentist();
		
		//compiler error - Doesn't follow IS-A
		Ortho	ortho=new	Dentist();
		
		Doctor	doctor2=new	KneeSurgeon();
		Ortho	ortho2=new KneeSurgeon();
		
		//compiler error - Doesn't follow IS-A
		KneeSurgeon	kneeSurgeon=new	Ortho();
		
		
	}
}