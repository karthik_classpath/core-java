##  Creating an Interface "PaymentGateway"
```java
public interface PaymentGateway {
    boolean transferAmount(String from, String to, String notes);
}
```
##  Class that implements 
```java
public class GooglePay implements PaymentGateway{

    @Override
    public boolean transferAmount(String from, String to, String notes) {
        System.out.println("Transfering money from "+ from +" to "+ to+", notes: "+ notes);
        System.out.println("You have unlocked two free movie tickets");
        return true;
    }

}
```
##  Another class that implements
```java
public class Paytm implements PaymentGateway, PhoneRecharge {
    @Override
    public boolean transferAmount(String from, String to, String notes) {
        System.out.println("Transfering money from "+ from +" to "+ to+", notes: "+ notes);
        System.out.println("You have been rewarded with 50 Rs cashback");
        return true;
    }
```
##  Client class
```java
public class PaymentClient {
    public static void main(String[] args) {
        //LHS = RHS
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please select an option ");
        System.out.println("Option 1: GooglePay");
        System.out.println("Option 2: PayTM");
        int option = scanner.nextInt();
        PaymentGateway gateway = null;
        switch (option){
            case 1:
                gateway = new GooglePay();
                break;
            case 2:
                gateway = new Paytm();
                break;
            default:
                gateway = new GooglePay();
        }

        boolean status = gateway.transferAmount("Alex", "xyz", "Bill payment");
        System.out.println("Status of funds transfer "+ status);
        scanner.close();
    }
}
```
## Creating another interface for multiple inheritance
```java
public interface PhoneRecharge {
    boolean recharge(String phoneNumber, double amount);
}
```
##  In GPay add
```java
@Override
public boolean recharge(String phoneNumber, double amount) {
    System.out.printf("Recharge for %s for the amount %f", phoneNumber, amount);
    return true;
}
```
In client class
```java
if (gateway instanceof PhoneRecharge){
    ((PhoneRecharge)gateway).recharge("9874545", 250);
}