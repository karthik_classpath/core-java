package io.classpath.exceptiondemo;

public class SavingsAccount {

    private double balance;

    public void deposit(double amount){
        this.balance = this.balance + amount;
    }

    public double withdraw(double amount) throws InsufiicientFundsException {
        if ( this.balance - amount <= 0 ){
            // invalid transaction
            //return 0;
            throw new InsufiicientFundsException(" Invlid amount passed ");
        } else { 
            this.balance = this.balance - amount;
            return amount;
        }
    }

    public double getBalance() {
        return this.balance;
    }

    public static void main(String[] args) {
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.deposit(40_000);
        double withdrawAmount = 0;
        try {
            withdrawAmount = savingsAccount.withdraw(20_000);
        } catch (InsufiicientFundsException e) {
            System.out.println(" You do not have sufficient funds to withdraw. Available balance is "+ savingsAccount.getBalance());
        }

        System.out.println(" Amount withdrawn "+ withdrawAmount);
        System.out.println(" Balance amount "+ savingsAccount.getBalance());

        double amountWithdrawn = 0; 
        try {
            amountWithdrawn = savingsAccount.withdraw(50_000);
        } catch (InsufiicientFundsException e) {
            System.out.println(" You do not have sufficient funds to withdraw. Available balance is "+ savingsAccount.getBalance());
        }
        System.out.println(" Now withdrawn amount is "+ amountWithdrawn);

        System.out.println("Balance amount is "+ savingsAccount.getBalance());
    }
}