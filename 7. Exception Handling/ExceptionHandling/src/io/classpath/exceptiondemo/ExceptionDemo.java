package io.classpath.exceptiondemo;

import java.util.Scanner;

public class ExceptionDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numbers[]= new int[]{11,22,33,44,55,66};
        System.out.println("Enter a number :: ");
        int option1 = scanner.nextInt();
        System.out.println("Enter the second number :: ");
        int option2 = scanner.nextInt();
        int result = 0;
        System.out.println("Please enter your fav  number  between 0 and " + numbers.length);
        int index = scanner.nextInt();

        try{
            result = option1 / option2;
            System.out.println(" THe number corresponding to the index is "+numbers[index]);
        } catch (ArithmeticException exception){
            System.out.println(" The second number cannot be zero");
        } catch (ArrayIndexOutOfBoundsException indexOutOfBoundsException){
            System.out.println(" The number should be between 0 and "+ numbers.length);
        } catch ( NullPointerException | IllegalArgumentException exception) {
            //handle exception in case of either null pointer or Illegal argument
        } catch (Exception e){
            if (e instanceof ArithmeticException) {
                System.out.println("There is something wrong. please try again....");
            }
        } finally {
            scanner.close();
        } 
        //any further statement will not be executed if there is an exception
        System.out.printf("The result is %d ", result );
    }
}