package io.classpath.exceptiondemo;

import java.io.*;

public class FileReaderDemo {

    public void readFile(String path) throws IOException {
        File file = new File(path);
        FileReader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        bufferedReader.readLine();
    }

    public static void main(String[] args) {
        FileReaderDemo fileReaderDemo = new FileReaderDemo();
        try {
            fileReaderDemo.readFile("abcd...");
        } catch (IOException e) {
            System.out.println(" Invalid file name");

        }
    }
}