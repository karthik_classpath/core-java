package io.classpath.exceptiondemo;

public class InsufiicientFundsException extends Exception {

    public InsufiicientFundsException(String message){
        super(message);
    }

}