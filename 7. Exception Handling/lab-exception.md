## For unchecked ;Create this class and run ; check the exception
```java
public class ExceptionDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numbers[]= new int[]{11,22,33,44,55,66};
        System.out.println("Enter a number :: ");
        int option1 = scanner.nextInt();
        System.out.println("Enter the second number :: ");
        int option2 = scanner.nextInt();
        int result = 0;
        result=option1/option2;
        System.out.printf("The result is %d ", result );
        scanner.close();
    }
}
```
##  Now use try and catch
```java
try{
    result=option1/option2;
}   
catch(ArithmeticException   exception){
    System.out.println("The second number cannot be zeero");
} 
```
##  Add an array and try to access beyond it's size
```java
int numbers[]= new int[]{11,22,33,44,55,66};
System.out.println("Please enter your fav  number  between 0 and " + numbers.length);
int index = scanner.nextInt();

try{
            result = option1 / option2;
            System.out.println(" THe number corresponding to the index is "+numbers[index]);
        } catch (ArithmeticException exception){
            System.out.println(" The second number cannot be zero");
```
## You can see that ArrayIndexOutOfBound Exception, so write another catch block for this.
```java
try{
    result = option1 / option2;
    System.out.println(" THe number corresponding to the index is "+numbers[index]);
} catch (ArithmeticException exception){
    System.out.println(" The second number cannot be zero");
} catch (ArrayIndexOutOfBoundsException indexOutOfBoundsException){
    System.out.println(" The number should be between 0 and "+ numbers.length);
}
```
##  Add finally block
```java
finally {
    scanner.close();
}
```
##  Now for checked exception, one way to handle is
```java
public class FileReaderDemo {

    public void readFile(String path) throws IOException {
        File file = new File(path);
        try{
            FileReader reader = new FileReader(file);
        }
        catch (FileNoutFoundException e) {
            System.out.println(" Invalid file name");

        }
    }

    public static void main(String[] args) {
        FileReaderDemo fileReaderDemo = new 
        fileReaderDemo.readFile("abcd...");
    }
}
```

##  Other way
```java
public class FileReaderDemo {

    public void readFile(String path) throws IOException {
        File file = new File(path);
        FileReader reader = new FileReader(file);
    }

    public static void main(String[] args) {
        FileReaderDemo fileReaderDemo = new FileReaderDemo();
        try {
            fileReaderDemo.readFile("abcd...");
        } catch (IOException e) {
            System.out.println(" Invalid file name");
        }
    }
}
```
##  Creating custom exception, create another class
```java
public class SavingsAccount {

    private double balance;

    public void deposit(double amount){
        this.balance = this.balance + amount;
    }

    public double withdraw(double amount) throws InsufiicientFundsException {
        if ( this.balance - amount <= 0 ){
            // invalid transaction
            return 0;
        } else {
            this.balance = this.balance - amount;
            return amount;
        }
    }

    public double getBalance() {
        return this.balance;
    } 

    public static void main(String[] args) {
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.deposit(40_000);
        
        double withdrawAmount = savingsAccount.withdraw(20_000);
        System.out.println(" Amount withdrawn "+ withdrawAmount);
        System.out.println(" Balance amount "+ savingsAccount.getBalance());

        double amountWithdrawn = savingsAccount.withdraw(50_000);
        System.out.println(" Now withdrawn amount is "+ amountWithdrawn);
        System.out.println("Balance amount is "+ savingsAccount.getBalance());
    }
}
```
##  Now we handle these exceptions instead of printing '0'
```java
throw new InsufiicientFundsException(" Invlid amount passed ");
```
This instead of  ` return    0`
##  Other way is to create a custom Exception class
```java
public class InsufiicientFundsException extends Exception {

    public InsufiicientFundsException(String message){
        super(message);
    }

}
```
##  Use try catch and handle
```java
package io.classpath.exceptiondemo;

public class SavingsAccount {

    private double balance;

    public void deposit(double amount){
        this.balance = this.balance + amount;
    }

    public double withdraw(double amount) throws InsufiicientFundsException {
        if ( this.balance - amount <= 0 ){
            // invalid transaction
            //return 0;
            throw new InsufiicientFundsException(" Invlid amount passed ");
        } else { 
            this.balance = this.balance - amount;
            return amount;
        }
    }

    public double getBalance() {
        return this.balance;
    }

    public static void main(String[] args) {
        SavingsAccount savingsAccount = new SavingsAccount();
        savingsAccount.deposit(40_000);
        double withdrawAmount = 0;
        try {
            withdrawAmount = savingsAccount.withdraw(20_000);
        } catch (InsufiicientFundsException e) {
            System.out.println(" You do not have sufficient funds to withdraw. Available balance is "+ savingsAccount.getBalance());
        }

        System.out.println(" Amount withdrawn "+ withdrawAmount);
        System.out.println(" Balance amount "+ savingsAccount.getBalance());

        double amountWithdrawn = 0;
        try {
            amountWithdrawn = savingsAccount.withdraw(50_000);
        } catch (InsufiicientFundsException e) {
            System.out.println(" You do not have sufficient funds to withdraw. Available balance is "+ savingsAccount.getBalance());
        }
        System.out.println(" Now withdrawn amount is "+ amountWithdrawn);

        System.out.println("Balance amount is "+ savingsAccount.getBalance());
    }
}
```


