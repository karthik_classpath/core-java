##  Overriding
```java
class	Doctor{
	public void treatPatient() {
		System.out.println("Treating patients");
	}
}

class	Dentist	extends	Doctor{
	public void toothImplant() {
		System.out.println("Implanting tooth");
	}
	@Override
	public void treatPatient() {
				System.out.println("Treating patients with tooth problems");
				toothImplant();
	}
}

class	Ortho	extends	Doctor{
	public void conductXRay() {
		System.out.println("Conducting XRay");
	}
	@Override
	public void treatPatient() {
		System.out.println("Treating patients with bone ailments");
		conductXRay();
	}
}
  
class	KneeSurgeon	extends	Ortho{
	public void kneeSurgery() {
		System.out.println("Performing knee surgery");
		kneeSurgery();
	}
    @Override
	public void treatPatient() {
		System.out.println("Treating patients with knee problems");
		kneeSurgery();
	}
}
```
## final keyword
```java
final	class	Doctor{
	final	public void treatPatient() {
		System.out.println("Treating patients");
	}
}

class	Dentist	extends	Doctor{
	public void toothImplant() {
		System.out.println("Implanting tooth");
	}
	@Override
	public void treatPatient() {
				System.out.println("Treating patients with tooth problems");
				toothImplant();
	}
}

class	Ortho	extends	Doctor{
	public void conductXRay() {
		System.out.println("Conducting XRay");
	}
	@Override
	public void treatPatient() {
		System.out.println("Treating patients with bone ailments");
		conductXRay();
	}
}
  
class	KneeSurgeon	extends	Ortho{
	public void kneeSurgery() {
		System.out.println("Performing knee surgery");
		kneeSurgery();
	}
	@Override
	public void treatPatient() {
		System.out.println("Treating patients with knee problems");
		kneeSurgery();
	}
}

public class OverridingDemo {
	public static void main(String[] args) {
		Doctor	doctor=new	Dentist();
		doctor.treatPatient();
		
		final	int	number=5;
		//Error because alreadt initialised
		int++;
	}
}
```
##  abstract
```java
abstract	class	Doctor{
	abstract	public void treatPatient();

}

class	Dentist	extends	Doctor{
	public void toothImplant() {
		System.out.println("Implanting tooth");
	}
	@Override
	public void treatPatient() {
				System.out.println("Treating patients with tooth problems");
				toothImplant();
	}
}

class	Ortho	extends	Doctor{
	public void conductXRay() {
		System.out.println("Conducting XRay");
	}
	@Override
	public void treatPatient() {
		System.out.println("Treating patients with bone ailments");
		conductXRay();
	}
}
```
##  Overloading
```java
public void selectedDoctor(String	doc,String	specialisation) {
		System.out.println("You have selected a "+doc+" who is an "+specialisation);
	}
	public void selectedDoctor(String	doc,String	specialisation,String	moreSpecial) {
		System.out.println("You have selected a "+doc+" who is an "+specialisation+" also a "+moreSpecial);
	}
	public void selectedDoctor(String doc, int regnum) {
		System.out.println("You have selected a "+doc+" with an id "+regnum);
	}

public static void main(String[] args) {
		KneeSurgeon kneeSurgeon=new	KneeSurgeon()
				;
		kneeSurgeon.selectedDoctor("doctor", 5);
		kneeSurgeon.selectedDoctor("doctor", "ortho");
		kneeSurgeon.selectedDoctor("doctor", "ortho", "knee surgeon");
		
	}
```    