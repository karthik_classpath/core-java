package io.classpath.packageone;


public class PublicClass {
	//only for the class, not even to the subclass
    private int privateVariable = 56;

    //This variable is visible only in the package
    int defautlValue = 45;


    //Visibility is in the package and subclasses outside the package
    protected int protectedVariable = 100;

    //accessible to everyone
    public int publicVariable = 450;

    public static final double PI = 3.142;

    public static void main(String[] args) {
        PublicClass obj = new PublicClass();
        System.out.printf("Private variable %d", obj.privateVariable);
        System.out.printf("Default variable %d", obj.defautlValue);
        System.out.printf("Protected variable %d", obj.protectedVariable);
        System.out.printf("Public variable %d", obj.publicVariable);
    }
}
