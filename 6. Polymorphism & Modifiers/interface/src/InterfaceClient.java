import java.util.Scanner;

public class InterfaceClient {
		 public static void main(String[] args) {
		        
		      	Scanner scanner = new Scanner(System.in);
		        System.out.println("Please select an option ");
		        System.out.println("Option 1: GooglePay");
		        System.out.println("Option 2: PayTM");
		        int option = scanner.nextInt();
		        PaymentGateway gateway = null;
		        switch (option){
		            case 1:
		                gateway = new GooglePay();
		                break;
		            case 2:
		                gateway = new PayTM();
		                break;
		            default:
		                gateway = new GooglePay();
		        }
		        boolean status = gateway.transferAmount("abc", "xyz", "Bill payment");
		        if (gateway instanceof PhoneRecharge){
		            ((PhoneRecharge)gateway).recharge("9874545", 250);
		        }
		        System.out.println("Status of funds transfer "+ status);
		        scanner.close();
		    }
}
