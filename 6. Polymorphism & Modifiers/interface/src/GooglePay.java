
public class GooglePay implements	PaymentGateway,	PhoneRecharge{

	 @Override
	    public boolean transferAmount(String from, String to, String notes) {
	        System.out.println("Transfering money from "+ from +" to "+ to+", notes: "+ notes);
	        System.out.println("You have unlocked two free movie tickets");
	        return true;
	    }

	    @Override
	    public boolean recharge(String phoneNumber, double amount) {
	        System.out.printf("Recharge for %s for the amount %f", phoneNumber, amount);
	        return true;
	    }
	
}
