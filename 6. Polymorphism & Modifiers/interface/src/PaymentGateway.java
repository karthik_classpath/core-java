
public interface PaymentGateway {
	boolean transferAmount(String from, String to, String notes);
}
