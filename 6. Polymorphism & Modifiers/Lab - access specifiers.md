##  Create a class in a package "packageone"
```java
package io.classpath.packageone;

public class PublicClass {

    private int privateVariable = 56;

    int defautlValue = 45;

    protected int protectedVariable = 100;

    public int publicVariable = 450;

    public static void main(String[] args) {
        PublicClass obj = new PublicClass();
        System.out.printf("Private variable %d", obj.privateVariable);
        System.out.printf("Default variable %d", obj.defautlValue);
        System.out.printf("Protected variable %d", obj.protectedVariable);
        System.out.printf("Public variable %d", obj.publicVariable);
    }

}
```
##  Another class in the same package
```java
package io.classpath.packagedemo;

public class DefaultClass {
    public static void main(String[] args) {
        PublicClass obj = new PublicClass();
       
        // System.out.printf("Private variable %d", obj.privateVariable);
        System.out.printf("Default variable %d", obj.defautlValue);
        System.out.printf("Protected variable %d", obj.protectedVariable);
        System.out.printf("Public variable %d", obj.publicVariable);  
    }
}
```
##  Create a new package "packagetwo" and class in it
```java
package io.classpath.packagetwo;
import io.classpath.packageone.PublicClass;

public class PackageClass extends io.classpath.packagedemo.PublicClass {
    public static void main(String[] args) {
        PublicClass obj = new io.classpath.packagedemo.PublicClass();

        
        // System.out.printf("Private variable %d", obj.privateVariable);
       // System.out.printf("Default variable %d", obj.defautlValue);
        //System.out.printf("Protected variable %d", obj.protectedVariable);

        PackageClass subObj = new PackageClass();
        System.out.printf("Protected variable %d ", subObj.protectedVariable);
        System.out.printf("Public variable %d", obj.publicVariable);
        
    }
}
```
##  Write fully qualified class name 
```java
public class PackageClass extends io.classpath.packagedemo.PublicClass {
    public static void main(String[] args) {
        io.classpath.packagedemo.PublicClass obj = new io.classpath.packagedemo.PublicClass();
```
##  Create a static variable "PublicClass"
```java
public static final double PI = 3.142;
```
## Import or use class name to access PI in "PackageClass"
```java
import static io.classpath.packagedemo.PublicClass.PI;

 System.out.println(" Public variable "+ PublicClass.PI);
 ```