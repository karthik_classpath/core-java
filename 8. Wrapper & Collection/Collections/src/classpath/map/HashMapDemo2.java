package classpath.map;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
 
public class HashMapDemo2 {
	
	public static void main(String[] args) {
		Map<Integer, String> empMap	=	new TreeMap<>(new	keySort());
		empMap.put(1	, "Alex");
		empMap.put(2	, "Adam");
		empMap.put(3	, "Alex");
		empMap.put(4	, "Ramesh");
		
		System.out.println(empMap.get(2));
		System.out.println(empMap.containsKey(3));
		System.out.println(empMap.containsValue("john"));
		
		Set<Integer> keySet = empMap.keySet();
		Iterator<Integer>	keyIterator	=	keySet.iterator();
		
		while (keyIterator.hasNext()) {
			System.out.println(keyIterator.next());
		}
		
		Collection<String>	valueCollection	=	empMap.values();
		Iterator<String> valueIterator	=	valueCollection.iterator();
		while (valueIterator.hasNext()) {
			System.out.println(valueIterator.next());
		}
		
		Set<Map.Entry<Integer, String>> entries	=	empMap.entrySet();
		Iterator<Map.Entry<Integer, String>>	entryIterator	=	entries.iterator();
		
		while (entryIterator.hasNext()) {
			Map.Entry<Integer, String>	entry	=	entryIterator.next();
			Integer	key = entry.getKey();
			String	value = entry.getValue();
			
			System.out.println("Key "+key+" Value "+value);
		}
		
		
	}
}

class	keySort	implements	Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		return o2-o1;
	}
	
}
