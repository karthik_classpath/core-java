package classpath.queue;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import classpath.set.Employee2;

public class PriorityQueueDemo2 {
	public static void main(String[] args) {
		Queue<Employee2>	queue	=	new	PriorityQueue<>(new	NameComparatorDesc());
		queue.add(new Employee2(90, "Alex"));
		queue.add(new Employee2(23, "Ram"));
		queue.add(new Employee2(4, "John")); 
		
		System.out.println(queue.peek());
	}
}

class	NameComparatorDesc	implements	Comparator<Employee2>{

	@Override
	public int compare(Employee2 o1, Employee2 o2) {
		// TODO Auto-generated method stub
		return o2.getName().compareTo(o1.getName());
	}
	
}
