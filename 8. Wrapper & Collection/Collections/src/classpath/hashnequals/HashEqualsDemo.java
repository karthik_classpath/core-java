package classpath.hashnequals;

public class HashEqualsDemo {
	public static void main(String[] args) {
		Employee	ram	=	new	Employee(1, "Ram", 25);
		Employee	vishnu	=	new	Employee(3, "Vishnu", 25);
		Employee	ram2	=	new	Employee(1, "Ram", 25);
		
		System.out.println("Is ram == vishnu "+	(ram.equals(vishnu)));
		System.out.println("Hashcode of Ram "+ ram.hashCode());
		System.out.println("Hashcode of Vishnu "+ vishnu.hashCode());
		System.out.println("Hashcode of Ram 2 "+ ram2.hashCode());
	}
}
