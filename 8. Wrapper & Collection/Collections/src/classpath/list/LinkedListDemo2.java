package classpath.list;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LinkedListDemo2 {
	public static void main(String[] args) {
		
	LinkedList<String> linkList	=	new LinkedList<>();
	
	linkList.add("varun");
    linkList.add("alex");
    linkList.add(0, "gautham");
    linkList.add("praveen");
    linkList.add("praveen");
    
    linkList.addFirst("newone");
    
    linkList.forEach((value)->System.out.println(value));
    
    System.out.println(linkList.getLast());
    System.out.println(linkList.getFirst());
    
    System.out.println(linkList.lastIndexOf("praveen"));
    
    System.out.println(linkList.peek());
    System.out.println();
    System.out.println(linkList.size());
    System.out.println(linkList.poll());
    System.out.println(linkList.size());
    
    
  	}
}