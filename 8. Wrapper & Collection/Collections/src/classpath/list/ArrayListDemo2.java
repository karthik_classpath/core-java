package classpath.list;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import classpath.set.Employee2;

public class ArrayListDemo2 {
	public static void main(String[] args) {
		
	int[]	numbers= {33,44,1};
	
		List<Integer> numList	=	new ArrayList<>();
		numList.add(56);
	    numList.add(42);
	    numList.add(0, 99);
	    numList.add(21);
	    
	    numList.remove(3);
	    
	    for (int index = 0; index < numList.size(); index++) {
			System.out.println(numList.get(index));
		}
		
	    for (Integer integer : numList) {
			System.out.println("Usinf enhanced loop "+integer);
		}
	    
	    numList.forEach((value)->System.out.println("Value using lambda "+value));
	    System.out.println((numList.indexOf(99)));
	    
	    ListIterator<Integer>	listIterator	=	numList.listIterator();
	    while (listIterator.hasNext()) {
			System.out.println(listIterator.next());
		}
	    while (listIterator.hasPrevious()) {
			System.out.println(listIterator.previous());
		}
	    
	}
}
