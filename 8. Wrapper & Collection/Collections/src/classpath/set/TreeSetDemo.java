package classpath.set;

import java.util.Iterator;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;



public class TreeSetDemo {
	public static void main(String[] args) {
		
		Set<Employee2> treeSet	=	new	TreeSet<>(new	IdComparatorDesc());
		
		treeSet.add(new Employee2(90, "Alex"));
		treeSet.add(new Employee2(23, "Ram"));
		treeSet.add(new Employee2(4, "John"));
		
		//treeSet.forEach((emp)->System.out.println(emp));
		
		Iterator<Employee2> iterator	=	treeSet.iterator();
		
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}

class	NameComparatorAsc	implements	Comparator<Employee2>{

	@Override
	public int compare(Employee2 o1, Employee2 o2) {
		// TODO Auto-generated method stub
		return o1.getName().compareTo(o2.getName());
	}
	
}

/*class	NameComparatorDesc	implements	Comparator<Employee2>{

	@Override
	public int compare(Employee2 o1, Employee2 o2) {
		// TODO Auto-generated method stub
		return o2.getName().compareTo(o1.getName());
	}
	
}*/

class	IdComparatorDesc	implements	Comparator<Employee2>{
	@Override
	public int compare(Employee2 o1, Employee2 o2) {
		// TODO Auto-generated method stub
		return (int)(o2.getId()-o1.getId());
	}
}