package classpath.set;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo2 {
	public static void main(String[] args) {
		Set<Employee2> hashSet	=	new	HashSet<>();
		
		hashSet.add(new Employee2(90, "Alex"));
		hashSet.add(new Employee2(90, "Alex"));
		hashSet.add(new Employee2(90, "Alex"));
		
		System.out.println(hashSet.size());
	}
}
