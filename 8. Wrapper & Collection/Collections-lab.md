#   ArrayList
##  Create an ArrayList and add few elements
```java
public static void main(String[] args) {
	
    List<Integer> numList	=	new ArrayList<>();
	numList.add(2);
	numList.add(56);
    numList.add(42);
    numList.add(21);
		
}
```
##  Basic for loop to iterate
```java
for (int index = 0; index < numList.size(); index++) {
	System.out.println(numList.get(index));
}
```
##  Iterate using lambda expression	
```java
  numList.forEach((value)->System.out.println("Value using lambda "+value));
```
##  Try the following methods
```java
numList.remove(3);
```
```java
System.out.println((numList.indexOf(99)));
System.out.println((numList.indexOf(8)));
```
```java
System.out.println((numList.contains(99)));
```
```java
numList.set(1,30);	
```
---------
<br>

#   LinkedList
##  Create a LinkedList same as ArrayList
```java
LinkedList<String> linkList	=	new LinkedList<>();

linkList.add("varun");
linkList.add("alex");
linkList.add(0, "gautham");
linkList.add("praveen");
```
## Iterate using the same 2 different techniques
##  Try the following methods
```java
linkList.addFirst("newone");
linkList.forEach((value)->System.out.println(value));
```
```java
System.out.println(linkList.getLast());
System.out.println(linkList.getFirst());
```
```java
linkList.add("praveen");
System.out.println(linkList.lastIndexOf("praveen"));
```
```java
System.out.println(linkList.peek());
 ```
 ```java
System.out.println(linkList.size());
System.out.println(linkList.poll());
System.out.println(linkList.size());
```
-----------
<br>

#   HashSet
##  Create a class Employee
```java
public class Employee2 {
	private	long	id;
	private	String	name;
}
```
##  Generate constructors, getters&setters, hashCode & equals
##  Create another class HashSetDemo
```java
Set<Employee2> hashSet	=	new	HashSet<>();

hashSet.add(new Employee2(90, "Alex"));
hashSet.add(new Employee2(90, "Alex"));
hashSet.add(new Employee2(90, "Alex"));

System.out.println(hashSet.size());
```
Size will be 1 because set cannot have duplicates.
##  Comment the overriden equals method and check size again

----------
<br>

#   TreeSet
##  Create class TreeSetDemo in same package of Employee
```java
Set<Employee2> treeSet	=	new	TreeSet<>();

treeSet.add(new Employee2(90, "Alex"));
treeSet.add(new Employee2(23, "Ram"));
treeSet.add(new Employee2(4, "John"));
```
##  In Employee class implement Comparable for the order - Default 
```java
@Override
public int compareTo(Employee2 employee2) {
	// TODO Auto-generated method stub
	return	(int)(this.id-employee2.id);
}
```
##	Generate toString for printing object and print them
```java
@Override
public String toString() {
	return "Employee2 [id=" + id + ", name=" + name + "]";
}
```
##	For multiple 'order'
##	Implement Comparator and create more classs in the client class
```java
class	NameComparatorAsc	implements	Comparator<Employee2>{
	@Override
	public int compare(Employee2 o1, Employee2 o2 {
		// TODO Auto-generated method stub
		return o1.getName().compareTo(o2.getName());
	}
	
}

class	NameComparatorDesc	implements	Comparator<Employee2>{
	@Override
	public int compare(Employee2 o1, Employee2 o2 {
		// TODO Auto-generated method stub
		return o2.getName().compareTo(o1.getName());
	}
	
}

class	NdComparatorDesc	implements	Comparator<Employee2>{
	@Override
	public int compare(Employee2 o1, Employee2 o2) {
		// TODO Auto-generated method stub
		return (int)(o2.getId()-o1.getId());
	}
}
```
##	Pick the suitable order while creating the Set
```java
Set<Employee2> treeSet	=	new	TreeSet<>(new	NameComparatorDesc());
```
##	Use Iterator to print
```java
Iterator<Employee2> iterator	=	treeSet.iterator();
while (iterator.hasNext()) {
	System.out.println(iterator.next());
}
```
-----
##	Use ListIterator for List
```java
ListIterator<Integer>	listIterator	=	numList.listIterator();
while (listIterator.hasNext()) {
	System.out.println(listIterator.next());
}
while (listIterator.hasPrevious()) {
	System.out.println(listIterator.previous());
}
```

