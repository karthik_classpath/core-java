##  Create a class
```java
public class PriorityQueueDemo2 {
	public static void main(String[] args) {
		Queue<Employee2>	queue	=	new	PriorityQueue<>();
		queue.add(new Employee2(90, "Alex"));
		queue.add(new Employee2(23, "Ram"));
		queue.add(new Employee2(4, "John")); 
		
		System.out.println(queue.peek());
	}
}
```
##  Similar to TreeSet you can change the priority(order) 
```java
Queue<Employee2>	queue	=	new	PriorityQueue<>(new	NameComparatorDesc());
```
##  Try poll method same as in LinkedList
-------
<br>

#   HashMap
##  Create a class for demo
```java
public class HashMapDemo2 {
	
	public static void main(String[] args) {
		Map<Integer, String> empMap	=	new HashMap<Integer, String>();
		empMap.put(1	, "Alex");
		empMap.put(2	, "Adam");
		empMap.put(3	, "Alex");
		empMap.put(4	, "Ramesh");
		
		System.out.println(empMap.get(2));
		
	}
}
```
##  Contains method
```java
System.out.println(empMap.containsKey(3));
System.out.println(empMap.containsValue("john"));
```
##  Retrieve keySet and print
```java
Set<Integer> keySet = empMap.keySet();
Iterator<Integer>	keyIterator	=	keySet.iterator();

while (keyIterator.hasNext()) {
	System.out.println(keyIterator.next());
}
```
##  Retrieve values and print
```java
Collection<String>	valueCollection	=	empMap.values();
Iterator<String> valueIterator	=	valueCollection.iterator();
while (valueIterator.hasNext()) {
	System.out.println(valueIterator.next());
}
```
##  Retrieve both key value together
```java
Set<Map.Entry<Integer, String>> entries	=	empMap.entrySet();
Iterator<Map.Entry<Integer, String>>	entryIterator	=	entries.iterator();
		
while (entryIterator.hasNext()) {
	Map.Entry<Integer, String>	entry	=	entryIterator.next();
	Integer	key = entry.getKey();
	String	value = entry.getValue();
			
	System.out.println("Key "+key+" Value "+value);
}
```
------
<br>

#   TreeMap
## Same as HashMap create a map add comparator
```java
Map<Integer, String> empMap	=	new TreeMap<>(new	keySort());
```
```java
class	keySort	implements	Comparator<Integer>{
	@Override
	public int compare(Integer o1, Integer o2) {
		// TODO Auto-generated method stub
		return o2-o1;
	}	
}
```


		