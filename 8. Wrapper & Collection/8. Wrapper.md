-   A wrapper class wraps around a primitive datatype and gives it an object appearance. 
-   Wrapper classes include methods to unwrap the object and give back the data type.
-   They implement toString, hashCode, equals
| PRIMTIVE TYPE|WRAPPER CLASS|
|:-----------:|:---------:|
double	|Double
float	|Float
long	|Long
int	|Integer
short	|Short
byte	|Byte
char	|Character
boolean	|Boolean

-   **Java Generics** works only with object and does not support primitive types.
-   Java Collections deal only with objects
-   When you want to refer **null** from data type, the you need object. Primitives cannot have null as value.
--------

##  There are two ways for converting a primitive type into an object of corresponding wrapper class 
```java
// 1. using constructor
Integer object = new Integer(10);
 
// 2. using static factory method
Integer anotherObject = Integer.valueOf(10);
```

##  Converting Wrapper Class to Primitive Type
We can use the corresponding instance methods to get the primitive type. e.g. intValue(), doubleValue(), shortValue() etc.
```java
Integer object = new Integer(10);
int val = object.intValue();   
```
------------
##  Autoboxing
-   Autoboxing is the automatic conversion of the primitive types into their corresponding wrapper class.
```java
Character ch = 'a';     //char to Character
```
Here compiler automatically creates an Character object from _ch_ and adds the object to integerList. <br> Thus, the previous code turn into the following at runtime
```java
Character ch = Character.valueOf('a')
```
##  Unboxing
Unboxing happens when the conversion happens from wrapper class to its corresponding primitive type.
```java
int sum = 0;
Integer i = 6;

if(i%2)
    sum += i;
```
In above example, the remainder (%) and unary plus (+=) operators does not apply on Integer objects. <br>The compiler automatically converts an Integer to an int at runtime by invoking the intValue() method.

-------

##  hashCode()
-   Returns a unique integer value for the object in runtime. 
-   It is by default an **int**.
-   This hash code is used for determining the bucket location, when this object needs to be stored in some HashTable like data structure.
-   The general contract of hashCode is: 
    -   Whenever it is invoked on the same object more than once duringan execution of a Java application, the hashCode method must consistently return the same integer, provided no information used in equals comparisons on the object is modified.This integer need not remain consistent from one execution of an application to another execution of the same application. 
    -   If two objects are equal according to the equals(Object)method, then calling the hashCode method on each of the two objects must produce the same integer result. 
    -   It is not required that if two objects are unequal according to the java.lang.Object.equals(java.lang.Object)method, then calling the hashCode method on each of thetwo objects must produce distinct integer results. However, the programmer should be aware that producing distinct integer resultsfor unequal objects may improve the performance of hash tables. 

##  equals(Object otherObject)
-   As method name suggests, is used to simply verify the equality of two objects.
-   It’s default implementation simply check the object references of two objects to verify their equality.
-   It is reflexive: for any non-null reference value x, x.equals(x) should return true. 
-   It is symmetric: for any non-null reference values x and y, x.equals(y)should return true if and only if y.equals(x) returns true. 
-   It is transitive: for any non-null reference values x, y, and z, if x.equals(y) returns true and y.equals(z) returns true, then x.equals(z) should return true. 
-    It is consistent: for any non-null reference values x and y, multiple invocations of x.equals(y) consistently return true or consistently return false, provided no information used in equals comparisons on the objects is modified. 
-   For any non-null reference value x, x.equals(null) should return false. 

##  Contract between hashCode() and equals()
It is generally necessary to override the hashCode() method whenever equals() method is overridden, so as to maintain the general contract for the hashCode() method, which states that **equal objects must have equal hash codes**.

