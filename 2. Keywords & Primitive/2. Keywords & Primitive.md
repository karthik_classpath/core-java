# Java Keywords
Java keywords are also known as reserved words. These are predefined words by Java so it cannot be used as a variable or object name.

### Number of keywords - 53
- Keywords - 50
    - Used keywords - 48
    - Reserved and not currently used - 2 (const, goto)
- Literals - 3 (true, false, null)

![](https://www.pixeltrice.com/wp-content/uploads/2020/07/keywords-in-Java.png)

----
##  final keyword
The final keyword makes a Java variable, method or class as final (it **cannot be changed** once it is initialized).

-   ### final variable
    A variable declared as final cannot be assigned another value after it has been initialized first time.
    ```java
    public final String VERSION = "1.0";
    ```
-   ### final method
    A final method cannot be overridden in child class. 
    ```java
    public class ParentClass {

        public final void showMyName() {
            System.out.println("In ParentClass");
        }
    
    }
-   ### final class
    We cannot inherit a final class.
    ```java
    public class final ParentClass  {   
        //fields and methods
    }
    ```
    <br>

    ![](https://techvidvan.com/tutorials/wp-content/uploads/sites/2/2020/06/Final-Keyword.jpg)


-------
-------
<br>

# Data types

Data types specify the different sizes and values that can be stored in the variable. There are two types of data types in Java:

![](https://media.geeksforgeeks.org/wp-content/cdn-uploads/20191105111644/Data-types-in-Java.jpg)

# Primitive Data Types
Primitive data types are the **building blocks** of data manipulation. These are the most basic data types available in Java language.


DATA TYPE   |	DESCRIPTION	| DEFAULT VALUE	| MEMORY SIZE
|----------|----------|---------|-------|
boolean	|A binary value of either true or false	|false	|1 bit
char	|Any unicode character|	\u0000 (0)|	16 bit unicode character
byte	|values from -128 to 127	|0|	8 bit signed value
short	|values from -32768 to 32767|	0|	16 bits signed value
int| values from from -2^31 to 2^31-1	|0	|32 bits signed value
long|	values values from from -2^63 to 2^63-1|	0	|64 bit floating point value
float|	IEEE 754 floating point	|0.0	|32 bit floating point value
double	|IEEE 754 floating point|	0.0|	64 bit floating point value

---
<br>

# Array
-   An array is a container object that holds a **fixed number of values** of a **single type** in a **contiguous** memory location.
-   Arrays are **index based** data structure so they allow **random access** to elements, they store. 

![](https://cdn.journaldev.com/wp-content/uploads/2017/11/java-array.png.webp)

##  Properties 
-   We can find the length of the array using attribute '**length**'.
-   Arrays are ordered and each have an index beginning from **'0'** for the first element.
-   Arrays can store primitives as well as objects.
-   Just like other variables, arrays can also be static, final or used as method arguments. 
-   The size of an array must be specified by an **int** value.

An array can be **one dimensional** or it can be **multidimensional**. Multidimensional arrays are in fact **arrays of arrays.**
![](https://cdn.journaldev.com/wp-content/uploads/2012/11/java-two-dimensional-array-diagram.png.webp)

----

##  Declaration


-   Declaring an array of primitive type of data (one dimensional)
```java
int[] integers;  // Recommended way of declaring an array
int  integers[]; // Legal but not recommended
```
-   Declaring an array of object type (one dimensional)
```java
String[] strings; 
```
-   Declaring multidimensional array
```java
int[][] integers; // Two dimensional array
String[][][] strings; // Three dimensional array
```
----
##  Initialize array
We can initialize an array using **new** keyword or using shortcut syntax, which creates and initialize array at the same time.
-  Initializing one dimensional array
```java
String[] strings; // declaration
strings = new String[5]; // initializing an array of type String object with size 5

int[] integers = new int[5]; // declaration and initialization in one line
```
-   Initializing multidimensional array:
```java
int[][] intArr = new int[4][5];

// multidimensional array initialization with only leftmost dimension
int[][] intArr = new int[2][];
intArr [0] = new int[2];
intArr [1] = new int[3]; // complete initialization is required before we use the array
```
-   Initializing an array using shortcut syntax:
```java
String[] strings = {"one", "two", "three"};
int[][] intArr2 = {{1, 2}, {1, 2, 3}};
```
-   Invalid ways to initialize an array
```java
int[] a = new int[]; // invalid because size is not provided
int[][] aa = new int[][5]; // invalid because leftmost dimension value is not provided
```
----

##  Accessing Array Elements

-   Array object have public variable called length, which gives the number of elements in the array.
-   We can process or traverse array elements using **for** or **foreach** loop.
```java
//Printing string array using for loop
for (int i = 0; i<strings.length; i++) {
	System.out.println(strings[i]); 
}

//Printing integer array using foreach loop
for (int i : integers) {
	System.out.println(i);
}
```
---
##  Printing Array elements
-   **Arrays.toString()** to print simple arrays
    ```java
    // An array of String objects
    String[] array = new String[] { "First", "Second", "Third", "Fourth" };

    // Print the array
    System.out.println( Arrays.toString(array) );   //[First, Second, Third, Fourth]
    ```
-   **Arrays.deepToString()** to print multi-dimensional array
    ```java
    int [][] cordinates = { {1,2}, {2,4}, {3,6,9} };
 
    System.out.println( Arrays.deepToString( cordinates ) );    //[[1, 2], [2, 4], [3, 6, 9]]
    ```
----

##  Java sort Array
Arrays can be sorted by using **java.util.Arrays** **sort** method which sorts the given array into an ascending order. 
```java
char[] chars = {'B', 'D', 'C', 'A', 'E'};
// sorting array of Character
	Arrays.sort(chars);
//A B C D E 
```
---

##  Copy Array
Object class provides **clone()** method and since an array in java is also an Object, you can use this method to achieve full array copy.
```JAVA
String[] strings = {"one", "two", "three", "four", "five"};
//Copy of strings array
String[] copyStrings = strings.clone();
```




